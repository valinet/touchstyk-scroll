﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TouchStykFix
{
    public partial class Form1 : Form
    {
        private static Point prevLoc;
        private static Point initial;
        private static Point final;
        private static bool wasScroll = false;

        [DllImport("user32.dll")]
        static extern IntPtr WindowFromPoint(System.Drawing.Point p);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr SetFocus(IntPtr hWnd);
        [DllImport("User32.dll", EntryPoint = "PostMessage")]
        private static extern int PostMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        static extern IntPtr GetFocus();
        [DllImport("kernel32.dll")]
        static extern uint GetCurrentThreadId();
        [DllImport("user32.dll")]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, IntPtr ProcessId);
        [DllImport("user32.dll")]
        static extern bool AttachThreadInput(uint idAttach, uint idAttachTo, bool fAttach);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, int cButtons, uint dwExtraInfo);

        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;
        private const int MOUSEEVENTF_WHEEL = 0x800;

        public Form1()
        {

            _hookID = SetHook(_proc);
            InitializeComponent();
            timer = new Timer();
            timer.Tick += Timer_Tick;
            timer.Interval = 10;
        }

        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;
        private const int WM_KEYUP = 0x0101;
        private static LowLevelKeyboardProc _proc = HookCallback;
        private static IntPtr _hookID = IntPtr.Zero;

        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                    GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

        private void Timer_Tick(object sender, EventArgs e)
        {
            int k = Cursor.Position.Y - prevLoc.Y;
            if (k > 0)
            {
                for (int i = 0; i < k; i = i + 5) // defines inertia
                {
                    mouse_event(MOUSEEVENTF_WHEEL, 0, 0, -20, 0); // how many lines
                    wasScroll = true;
                }
            }
            else if (k < 0)
            {
                k = Math.Abs(k);
                for (int i = 0; i < k; i = i + 5)
                {
                    mouse_event(MOUSEEVENTF_WHEEL, 0, 0, 20, 0);
                    wasScroll = true;

                }
            }
            Cursor.Position = prevLoc;
        }

        private static Timer timer;
        private static bool disabled = false;

        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            int vkCode = Marshal.ReadInt32(lParam);
            if (!disabled)
            {
                if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN && (Keys)vkCode == Keys.Space)
                {
                    if (!wasScroll && timer.Enabled)
                    {
                        timer.Enabled = false;
                        disabled = true;
                    }
                    else
                    {
                        if (timer.Enabled == false)
                        {
                            prevLoc = Cursor.Position;
                            initial = Cursor.Position;
                            wasScroll = false;
                            timer.Enabled = true;
                        }
                    }
                    return (IntPtr)1;
                }
                else if (nCode >= 0 && wParam == (IntPtr)WM_KEYUP && (Keys)vkCode == Keys.Space)
                {
                    final = Cursor.Position;
                    timer.Enabled = false;
                    if (!wasScroll)
                    {
                        disabled = true;
                        uint appThread = GetCurrentThreadId();
                        uint foregroundThread = GetWindowThreadProcessId(GetForegroundWindow(), IntPtr.Zero);

                        if (foregroundThread != appThread)
                        {
                            AttachThreadInput(foregroundThread, appThread, true);
                        }
                        Keys key = Keys.Space;
                        PostMessage(GetFocus(), WM_KEYDOWN, (uint)key, 0);
                        disabled = false;
                    }
                    return (IntPtr)1;
                }
            }
            if (nCode >= 0 && wParam == (IntPtr)WM_KEYUP && (Keys)vkCode == Keys.Space)
            {
                disabled = false;
            }
            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            UnhookWindowsHookEx(_hookID);
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            this.Hide();
            timer1.Enabled = false;
        }

        //
    }
}
