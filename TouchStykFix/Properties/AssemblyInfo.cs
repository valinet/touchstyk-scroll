﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TouchStyk Scroll")]
[assembly: AssemblyDescription("TouchStyk Scroll")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ValiNet")]
[assembly: AssemblyProduct("TouchStyk Scroll")]
[assembly: AssemblyCopyright("Copyright © 2006-2016 ValiNet Romania. All rights reserved.")]
[assembly: AssemblyTrademark("ValiNet")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e68c579d-2be3-489c-b63d-6b53b1ff46ad")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2016.10.17.0")]
[assembly: AssemblyFileVersion("2016.10.17.0")]
