TouchStyk Scroll attempts to fix a  big design flaw of the TouchStyk (TrackPoint clone) found on HP notebooks: it sets the space bar to act instead of the missing third button, whicha llows for scrolling in applications when held down.

Installation
* Just compile and run the app. Due to own constraints, it is very enterprise friendly (does not require administrator access to run).

Code
* App is written in C# using VS Express 2015 for Windows Desktop. Various codee xamples come from StackOverflow, and some other sites.

TODO:
* Code comments
* GUI to configure scrolling speed, inertia, etc.